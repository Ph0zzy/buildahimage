#!/usr/bin/env bash
# NEWCONTAINER=$(buildah from --volume /opt/atlassian/bitbucketci/agent/build:/bitbucketci:rw --volume /opt/atlassian/pipelines/agent/build:/pipelines:rw scratch)
NEWCONTAINER=$(buildah from --volume ${BITBUCKET_CLONE_DIR}:/var/lib/containers/storage:rw scratch)
SCRATCHMNT=$(buildah mount ${NEWCONTAINER})
dnf install --installroot ${SCRATCHMNT} bash coreutils dnf fuse-overlayfs buildah --releasever ${RELEASE} --setopt=tsflags=nodocs --setopt=install_weak_deps=False --setopt=override_install_langs=en_US.utf8 -y
if [ -f ${SCRATCHMNT}/etc/containers/storage.conf ]; then sed 's|^#mount_program|mount_program|g' -i ${SCRATCHMNT}/etc/containers/storage.conf; fi
if [ -d ${SCRATCHMNT} ]; then rm -rf ${SCRATCHMNT}/var/cache/dnf; fi
buildah config --env _BUILDAH_STARTED_IN_USERNS="" ${NEWCONTAINER}
buildah config --env BUILDAH_ISOLATION=chroot ${NEWCONTAINER}
buildah config --label name=${CI_PROJECT_NAME} ${NEWCONTAINER}
buildah config --cmd /bin/bash ${NEWCONTAINER}
buildah unmount ${NEWCONTAINER}
buildah commit ${NEWCONTAINER} ${CI_PROJECT_NAME}
buildah tag ${CI_PROJECT_NAME} ${CI_REGISTRY_IMAGE}:latest
buildah tag ${CI_PROJECT_NAME} ${CI_REGISTRY_IMAGE}:${RELEASE}
buildah --debug push --creds ${CI_REGISTRY_USER}:${CI_JOB_TOKEN} ${CI_REGISTRY_IMAGE}:${RELEASE}
buildah --debug push --creds ${CI_REGISTRY_USER}:${CI_JOB_TOKEN} ${CI_REGISTRY_IMAGE}:latest
